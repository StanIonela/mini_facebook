import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { JwtService } from "@nestjs/jwt";
import { UserRepositoryCreate } from "../user/dto/repository/create-user.repository";
import { UserRepositoryLogin } from "../user/dto/repository/login-user.repository";
import { UserEntity } from "../user/entities/user.entity";
import { AuthRepositoryRegistration } from "./dto/repository/auth-registration.repository";
import { AuthRepositoryLogin } from "./dto/repository/login-user.repository";

@Injectable()
export class AuthService{
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    private readonly jwtService:  JwtService,
  ) {}

  async register(createUserInput: AuthRepositoryRegistration): Promise<UserEntity> {
    const user = await this.userRepository.create(createUserInput);
    return await this.userRepository.save(user);
  }

  async validateUser(email: string, pass: string): Promise<any> {
    const user = await this.findByEmail(email);
    if (user && user.password === pass) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }

  async findByEmail(email: string): Promise<UserEntity> {
    return await this.userRepository.findOne({ where: { email } });
  }

  async login(data: AuthRepositoryLogin) {
    const payload = {email: data.email, password: data.password};
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}