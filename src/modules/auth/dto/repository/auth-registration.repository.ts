import { DateScalar } from "../../../../scalar/date.scalar";
import { GenderEnum } from "../../../user/enum/gender.enum";

export class AuthRepositoryRegistration {
  id?: string;
  firstName!: string;
  lastName!: string;
  email!: string;
  password!: string;
  avatar?: string;
  phone?: string;
  dateOfBirth?: DateScalar;
  city?: string;
  gender?: GenderEnum;
}
