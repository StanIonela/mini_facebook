import { Field, ObjectType } from '@nestjs/graphql';
import {
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsOptional, IsPhoneNumber,
  IsString, IsStrongPassword, IsUrl,
  IsUUID,
  Length
} from "class-validator";
import { DateScalar } from "../../../../scalar/date.scalar";
import { GenderEnum } from "../../../user/enum/gender.enum";

@ObjectType('Auth')
export class AuthHttp {
  @Field(() => String, { description: 'id of the user', nullable: true })
  @IsUUID()
  id?: string;

  @Field(() => String, {
    description: 'first name of the user',
    nullable: false,
  })
  @IsString()
  @IsNotEmpty()
  firstName!: string;

  @Field(() => String, {
    description: 'last name of the user',
    nullable: false,
  })
  @IsString()
  @IsNotEmpty()
  lastName!: string;

  @Field(() => String, { description: 'email of the user', nullable: false })
  @IsString()
  @IsEmail()
  @IsNotEmpty()
  email!: string;

  @Field(() => String, { description: 'password of the user' })
  @IsString()
  @IsNotEmpty()
  @Length(6)
  @IsStrongPassword()
  password!: string;

  @Field(() => String, { description: 'avatar of the user', nullable: true })
  @IsOptional()
  @IsUrl()
  avatar?: string;

  @Field(() => String, {
    description: 'phone number of the user',
    nullable: true,
  })
  @IsOptional()
  @IsString()
  @IsPhoneNumber()
  phone?: string;

  @Field(() => Date, {
    description: 'date of birth of the user',
    nullable: true,
  })
  @IsOptional()
  dateOfBirth?: DateScalar;

  @Field(() => String, {
    description: 'city from where user is',
    nullable: true,
  })
  @IsString()
  @IsOptional()
  city?: string;

  @Field(() => GenderEnum, { description: 'gender of the user', nullable: true })
  @IsEnum(GenderEnum)
  @IsOptional()
  gender?: GenderEnum;
}
