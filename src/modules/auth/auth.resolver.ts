import { Args, Mutation, Resolver } from "@nestjs/graphql";
import { UserHttp } from "../user/dto/http/user.http";
import { UserHttpCreate } from "../user/dto/http/create-user.http";
import { ValidationPipe } from "@nestjs/common";
import { UserHttpLogin } from "../user/dto/http/login-user.http";
import { AuthService } from "./auth.service";
import { AuthHttp } from "./dto/http/auth.http";
import { AuthHttpRegistration } from "./dto/http/auth-registration.http";
import { AuthHttpLogin } from "./dto/http/auth-login.http";

@Resolver(() => AuthHttp)
export class AuthResolver{
  constructor(private readonly authService: AuthService) {}

  @Mutation(() => AuthHttp)
  async createUser(@Args('data', { type: () => AuthHttpRegistration }, ValidationPipe) data: AuthHttpRegistration) {
    return await this.authService.register(data);
  }

  @Mutation(() => AuthHttp)
  async login(@Args('data', {type: () => AuthHttpLogin}, ValidationPipe) data: AuthHttpLogin) {
    return await this.authService.login(data);
  }
}