import { Field, InputType } from '@nestjs/graphql';
import {
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';
import { StatusEnum } from '../../enum/status.enum';

@InputType()
export class PostHttpCreate {
  @Field(() => String, { description: 'id of the post', nullable: true })
  id!: string;

  @Field(() => String, {
    description: 'title of the post',
    nullable: true,
  })
  @IsString()
  @IsOptional()
  title?: string;

  @Field(() => String, {
    description: 'description of the post',
    nullable: false,
  })
  @IsString()
  @IsNotEmpty()
  description!: string;

  @Field(() => StatusEnum, {
    description: 'status of the user',
    nullable: false,
  })
  @IsEnum(StatusEnum)
  @IsOptional()
  status?: StatusEnum;

  @Field(() => String)
  userId!: string;
}
