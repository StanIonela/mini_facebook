import { Field, InputType } from "@nestjs/graphql";
import { IsEnum, IsOptional, IsString } from "class-validator";
import { StatusEnum } from "../../enum/status.enum";

@InputType()
export class PostHttpUpdate {
  @Field(() => String, {
    description: 'title of the post',
    nullable: true,
  })
  @IsString()
  @IsOptional()
  title?: string;

  @Field(() => String, {
    description: 'description of the post',
    nullable: true,
  })
  @IsString()
  @IsOptional()
  description?: string;

  @Field(() => StatusEnum, {
    description: 'status of the user',
    nullable: true,
  })
  @IsEnum(StatusEnum)
  @IsOptional()
  status?: StatusEnum;
}
