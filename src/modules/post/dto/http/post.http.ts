import { Field, ObjectType } from '@nestjs/graphql';
import {
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';
import { StatusEnum } from '../../enum/status.enum';

@ObjectType('Post')
export class PostHttp {
  @Field(() => String, { description: 'id of the post', nullable: true })
  @IsUUID()
  id!: string;

  @Field(() => String, {
    description: 'title of the post',
    nullable: true,
  })
  @IsString()
  @IsOptional()
  title?: string;

  @Field(() => String, {
    description: 'description of the post',
    nullable: false,
  })
  @IsString()
  @IsNotEmpty()
  description!: string;

  @Field(() => String, {
    description: 'status of the user',
    nullable: false,
  })
  @IsEnum(StatusEnum)
  @IsOptional()
  status?: StatusEnum;
}
