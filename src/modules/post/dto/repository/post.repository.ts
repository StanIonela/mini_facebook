import { StatusEnum } from '../../enum/status.enum';

export class PostRepository {
  id!: string;
  title?: string;
  description!: string;
  status?: StatusEnum;
}
