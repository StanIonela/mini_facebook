import { StatusEnum } from "../../enum/status.enum";

export class PostRepositoryUpdate {
  title?: string;
  description?: string;
  status?: StatusEnum;
}
