import { StatusEnum } from '../../enum/status.enum';

export class PostRepositoryCreate {
  id!: string;
  title?: string;
  description!: string;
  status?: StatusEnum;
}
