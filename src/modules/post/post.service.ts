import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { PostEntity } from "./entities/post.entity";
import { PostRepositoryCreate } from "./dto/repository/create-post.repository";
import { PostRepositoryUpdate } from "./dto/repository/update-post.repository";
import { UserEntity } from "../user/entities/user.entity";

@Injectable()
export class PostService {
  constructor(
    @InjectRepository(PostEntity)
    private readonly postRepository: Repository<PostEntity>,
  ) {}

  async create(data: PostRepositoryCreate): Promise<PostEntity> {
    const post = await this.postRepository.create(data);
    return await this.postRepository.save(post);
  }

  async findAll(): Promise<PostEntity[]> {
    return await this.postRepository.find();
  }

  async findById(id: string) {
    return await this.postRepository.findOne({ where: { id } });
  }

  async findUser(postId: string): Promise<UserEntity> {
    const findUser = await this.postRepository.findOne({ where: {id: postId }, relations: {user: true}, select: ['id', 'user']});
    return findUser.user;
  }

  async updatePost(id: string, data: PostRepositoryUpdate) {
    const post = await this.postRepository.preload({
      id,
      ...data,
    });
    return this.postRepository.save(post);
  }

  async deletePost(id: string) {
    const post = await this.postRepository.findOne({ where: { id } });
    return await this.postRepository.remove(post);
  }
}
