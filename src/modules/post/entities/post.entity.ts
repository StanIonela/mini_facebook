import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, RelationId } from "typeorm";
import { StatusEnum } from '../enum/status.enum';
import { CommentEntity } from "../../comment/entities/comment.entity";
import { UserEntity } from "../../user/entities/user.entity";

@Entity()
export class PostEntity {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @Column({ nullable: true, type: 'varchar' })
  title?: string;

  @Column({ nullable: false, type: 'text' })
  description: string;

  @Column({
    type: 'enum',
    enum: StatusEnum,
    default: 'none',
  })
  status?: StatusEnum;

  @ManyToOne(() => UserEntity, (user) => user.posts)
  @JoinColumn({ name: 'userId' })
  user: UserEntity;

  @Column({ nullable: false })
  userId: string;

  @OneToMany(
    () => CommentEntity,
    (comment )=> comment.post,
    {cascade: true}
  )
  comments?: CommentEntity[];
}
