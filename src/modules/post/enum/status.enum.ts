import { registerEnumType } from "@nestjs/graphql";

export enum StatusEnum {
  NONE = 'none',
  HAPPY = 'happy',
  SAD = 'sad',
  ANGRY = 'angry',
  COOL = 'cool',
  TIRED = 'tired',
}

registerEnumType(StatusEnum, {
  name: 'StatusEnum'
})
