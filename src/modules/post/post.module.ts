import { Module } from '@nestjs/common';
import { PostService } from './post.service';
import { PostResolver } from './post.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PostEntity } from "./entities/post.entity";
import { PostRepository } from "./dto/repository/post.repository";
import { UserModule } from "../user/user.module";

@Module({
  imports: [TypeOrmModule.forFeature([PostEntity, PostRepository]), UserModule],
  providers: [PostResolver, PostService,],
})
export class PostModule {}
