import { Args, Mutation, Parent, Query, ResolveField, Resolver } from "@nestjs/graphql";
import { PostService } from './post.service';
import { PostEntity } from './entities/post.entity';
import { PostHttpCreate } from './dto/http/create-post.http';
import { PostHttpUpdate } from './dto/http/update-post.http';
import { PostHttp } from './dto/http/post.http';
import { ParseUUIDPipe, ValidationPipe } from "@nestjs/common";
import { UserHttp } from "../user/dto/http/user.http";
import { CommentHttp } from "../comment/dto/http/coment.http";
import { NotEmptyObjectPipe } from "../../validation/not-empty-object.pipe";

@Resolver(() => PostHttp)
export class PostResolver {
  constructor(private readonly postService: PostService) {}

  @Mutation(() => PostHttp)
  async createPost(@Args('data', { type: () => PostHttpCreate }, ValidationPipe) data: PostHttpCreate): Promise<PostEntity> {
    return await this.postService.create(data);
  }

  @Query(() => [PostHttp], { name: 'posts' })
  async findAll(): Promise<PostEntity[]> {
    return await this.postService.findAll();
  }

  @Query(() => PostHttp, { name: 'post' })
  async findOne(@Args('id', { type: () => String }, ParseUUIDPipe) id: string) {
    return await this.postService.findById(id);
  }

  @Mutation(() => PostHttp)
  async updatePost(
    @Args('id', { type: () => String }, ParseUUIDPipe) id: string,
    @Args('data', { type: () => PostHttpUpdate }, ValidationPipe, NotEmptyObjectPipe) data: PostHttpUpdate,) {
    return await this.postService.updatePost(id, data);
  }

  @Mutation(() => PostHttp)
  async removePost(@Args('id', { type: () => String }, ParseUUIDPipe) id: string) {
    return await this.postService.deletePost(id);
  }

  @ResolveField(() => UserHttp, {
    name: 'user',
    nullable: false,
    description: "User of comment"
  })
  resolveUser(
    @Parent() comment: CommentHttp
  ) {
    return this.postService.findUser(comment.id);
  }
}
