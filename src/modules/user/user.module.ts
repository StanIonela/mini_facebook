import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserResolver } from './user.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from "./entities/user.entity";
import { DateScalar } from '../../scalar/date.scalar';
import { UserRepository } from "./dto/repository/user.repository";
import { JwtService } from "@nestjs/jwt";

@Module({
  imports: [TypeOrmModule.forFeature([UserEntity, UserRepository])],
  providers: [UserResolver, UserService, DateScalar, UserRepository, JwtService],
  exports: [TypeOrmModule]
})
export class UserModule {}
