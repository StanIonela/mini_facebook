import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { UserService } from './user.service';
import { UserEntity } from './entities/user.entity';
import { UserHttpCreate } from './dto/http/create-user.http';
import { UserHttpUpdate } from './dto/http/update-user.http';
import { UserHttp } from './dto/http/user.http';
import { ParseUUIDPipe, UseGuards, ValidationPipe } from "@nestjs/common";
import { NotEmptyObjectPipe } from "../../validation/not-empty-object.pipe";
import { AuthGuard } from "@nestjs/passport";
import { UserHttpLogin } from "./dto/http/login-user.http";

@Resolver(() => UserEntity)
export class UserResolver {
  constructor(private readonly userService: UserService) {}

  // @UseGuards(AuthGuard('jwt'))
  @Query(() => [UserHttp], { name: 'users' })
  async findAll(): Promise<UserEntity[]> {
    return await this.userService.findAll();
  }

  @Query(() => UserHttp, { name: 'user' })
  async findOne(@Args('id', { type: () => String }, ParseUUIDPipe) id: string): Promise<UserEntity> {
    return await this.userService.findById(id);
  }

  @Mutation(() => UserHttp)
  async updateUser(
    @Args('id', { type: () => String }, ParseUUIDPipe) id: string,
    @Args('data', { type: () => UserHttpUpdate }, ValidationPipe, NotEmptyObjectPipe) data: UserHttpUpdate,
  ): Promise<UserEntity> {
    return await this.userService.updateUser(id, data);
  }

  @Mutation(() => UserHttp)
  async removeUser(
    @Args('id', { type: () => String }, ParseUUIDPipe ) id: string,
  ): Promise<UserEntity> {
    return await this.userService.deleteUser(id);
  }
}
