import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { UserRepositoryUpdate } from "./dto/repository/update-user.repository";
import { UserEntity } from "./entities/user.entity";

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
  ) {}

  async findAll(): Promise<UserEntity[]> {
    return await this.userRepository.find();
  }

  async findById(id: string): Promise<UserEntity> {
    return await this.userRepository.findOne({ where: { id } });
  }

  async updateUser(id: string, data: UserRepositoryUpdate): Promise<UserEntity> {
    const user = await this.userRepository.preload({
      id,
      ...data,
    });
    return this.userRepository.save(user);
  }

  async deleteUser(id: string): Promise<UserEntity> {
    const user = await this.userRepository.findOne({ where: { id } });
    return await this.userRepository.remove(user);
  }
}
