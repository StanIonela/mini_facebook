import { Column, Entity, OneToMany, PrimaryGeneratedColumn, Timestamp } from "typeorm";
import { DateScalar } from "../../../scalar/date.scalar";
import { PostEntity } from "../../post/entities/post.entity";
import { CommentEntity } from "../../comment/entities/comment.entity";
import { GenderEnum } from "../enum/gender.enum";

@Entity()
export class UserEntity {
  @PrimaryGeneratedColumn('uuid')
  id?: string;

  @Column({ nullable: false, type: 'varchar' })
  firstName!: string;

  @Column({ nullable: false, type: 'varchar' })
  lastName!: string;

  @Column({ unique: true, nullable: false, type: 'varchar' })
  email!: string;

  @Column({ nullable: false, type: 'varchar' })
  password!: string;

  @Column({ nullable: true, type: 'varchar' })
  avatar?: string;

  @Column({ nullable: true, type: 'varchar' })
  phone?: string;

  @Column({ type: 'varchar', nullable: true })
  dateOfBirth?: DateScalar;

  @Column({ nullable: true, type: 'varchar' })
  city?: string;

  @Column({
    type: 'enum',
    enum: GenderEnum,
    default: 'non_binary',
  })
  gender?: GenderEnum;

  @OneToMany(
    () => PostEntity,
    (post )=> post.user,
    {cascade: true}
  )
  posts?: PostEntity[];

  @OneToMany(
    () => CommentEntity,
    (comment )=> comment.user,
    {cascade: true}
  )
  comments?: CommentEntity[];
}
