import { registerEnumType } from "@nestjs/graphql";

export enum GenderEnum {
  MALE = 'male',
  FEMALE = 'female',
  NON_BINARY = 'non_binary',
}

registerEnumType(GenderEnum, {
  name: 'GenderEnum'
})
