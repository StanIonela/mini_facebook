import { Field, InputType } from '@nestjs/graphql';
import {
  IsEmail,
  IsNotEmpty,
  IsString, IsStrongPassword, IsUrl,
  Length
} from "class-validator";

@InputType()
export class UserHttpLogin {
  @Field(() => String, { description: 'email of the user', nullable: false })
  @IsString()
  @IsEmail()
  @IsNotEmpty()
  email!: string;

  @Field(() => String, { description: 'password of the user' })
  @IsString()
  @IsNotEmpty()
  @Length(6)
  @IsStrongPassword()
  password!: string;
}
