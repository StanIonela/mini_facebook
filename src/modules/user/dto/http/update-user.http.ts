import { Field, PartialType, InputType } from '@nestjs/graphql';
import { UserHttpCreate } from './create-user.http';
import {
  IsEmail, IsEnum,
  IsNotEmpty,
  IsOptional,
  IsPhoneNumber,
  IsString,
  IsStrongPassword,
  IsUrl,
  IsUUID,
  Length
} from "class-validator";
import { DateScalar } from "../../../../scalar/date.scalar";
import { GenderEnum } from "../../enum/gender.enum";

@InputType()
export class UserHttpUpdate{
  @Field(() => String, {
    description: 'first name of the user',
    nullable: true,
  })
  @IsString()
  @IsOptional()
  firstName?: string;

  @Field(() => String, {
    description: 'last name of the user',
    nullable: true,
  })
  @IsString()
  @IsOptional()
  lastName?: string;

  @Field(() => String, { description: 'email of the user', nullable: true })
  @IsString()
  @IsEmail()
  @IsOptional()
  email?: string;

  @Field(() => String, { description: 'password of the user', nullable: true })
  @IsString()
  @IsOptional()
  @Length(6)
  @IsStrongPassword()
  password?: string;

  @Field(() => String, { description: 'avatar of the user', nullable: true })
  @IsOptional()
  @IsUrl()
  avatar?: string;

  @Field(() => String, {
    description: 'phone number of the user',
    nullable: true,
  })
  @IsOptional()
  @IsString()
  @IsPhoneNumber()
  phone?: string;

  @Field(() => Date, {
    description: 'date of birth of the user',
    nullable: true
  })
  @IsOptional()
  dateOfBirth?: DateScalar;

  @Field(() => String, {
    description: 'city from where user is',
    nullable: true,
  })
  @IsString()
  @IsOptional()
  city?: string;

  @Field(() => String, { description: 'gender of the user', nullable: true })
  @IsEnum(GenderEnum)
  @IsOptional()
  gender?: GenderEnum;
}
