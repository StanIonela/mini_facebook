import { DateScalar } from "../../../../scalar/date.scalar";
import { GenderEnum } from "../../enum/gender.enum";

export class UserRepositoryUpdate {
  firstName?: string;
  lastName?: string;
  email?: string;
  password?: string;
  avatar?: string;
  phone?: string;
  dateOfBirth?: DateScalar;
  city?: string;
  gender?: GenderEnum;
}
