import { GenderEnum } from '../../enum/gender.enum';
import { DateScalar } from "../../../../scalar/date.scalar";

export class UserRepositoryCreate {
  id?: string;
  firstName!: string;
  lastName!: string;
  email!: string;
  password!: string;
  avatar?: string;
  phone?: string;
  dateOfBirth?: DateScalar;
  city?: string;
  gender?: GenderEnum;
}
