import { Injectable } from '@nestjs/common';
import { CommentRepositoryCreate } from './dto/repository/create-comment.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CommentEntity } from './entities/comment.entity';
import { CommentRepositoryUpdate } from './dto/repository/update-comment.repository';
import { UserEntity } from "../user/entities/user.entity";
import { PostEntity } from "../post/entities/post.entity";

@Injectable()
export class CommentService {
  constructor(
    @InjectRepository(CommentEntity)
    private readonly commentRepository: Repository<CommentEntity>,
  ) {}
  async createComment(data: CommentRepositoryCreate): Promise<CommentEntity> {
    const comment = await this.commentRepository.create(data);
    return await this.commentRepository.save(comment);
  }

  async findAll(): Promise<CommentEntity[]> {
    return  await this.commentRepository.find();
  }

  async findPost(commentId: string): Promise<PostEntity> {
    const findPost = await this.commentRepository.findOne({ where: {id: commentId }, relations: {post: true}, select: ['id', 'post']});
    return findPost.post;
  }

  async findUser(commentId: string): Promise<UserEntity> {
    const findUser = await this.commentRepository.findOne({ where: {id: commentId }, relations: {user: true}, select: ['id', 'user']});
    return findUser.user;
  }

  async findById(id: string): Promise<CommentEntity> {
    return await this.commentRepository.findOne({ where: { id } });
  }

  async updateComment(
    id: string,
    data: CommentRepositoryUpdate,
  ): Promise<CommentEntity> {
    const comment = await this.commentRepository.preload({
      id,
      ...data,
    });
    return await this.commentRepository.save(comment);
  }

  async deleteComment(id: string): Promise<CommentEntity> {
    const comment = await this.commentRepository.findOne({
      where: { id },
    });
    return await this.commentRepository.remove(comment);
  }
}
