import { Resolver, Query, Mutation, Args, ResolveField, Parent } from "@nestjs/graphql";
import { CommentService } from './comment.service';
import { CommentEntity } from './entities/comment.entity';
import { CommentHttpCreate } from './dto/http/create-comment.http';
import { CommentHttpUpdate } from './dto/http/update-comment.http';
import { CommentHttp } from './dto/http/coment.http';
import { ParseUUIDPipe, ValidationPipe } from "@nestjs/common";
import { PostHttp } from "../post/dto/http/post.http";
import { UserHttp } from "../user/dto/http/user.http";
import { NotEmptyObjectPipe } from "../../validation/not-empty-object.pipe";

@Resolver(() => CommentHttp)
export class CommentResolver {
  constructor(private readonly commentService: CommentService) {}

  @Mutation(() => CommentHttp)
  createComment(@Args('data', { type: () => CommentHttpCreate }, ValidationPipe) data: CommentHttpCreate): Promise<CommentEntity> {
    return this.commentService.createComment(data);
  }

  @Query(() => [CommentHttp], { name: 'comments' })
  findAll(): Promise<CommentEntity[]> {
    return this.commentService.findAll();
  }

  @Query(() => CommentHttp, { name: 'comment' })
  findOne(@Args('id', { type: () => String }, ParseUUIDPipe) id: string): Promise<CommentEntity> {
    return this.commentService.findById(id);
  }

  @Mutation(() => CommentHttp)
  updateComment(
    @Args('id', { type: () => String }, ParseUUIDPipe) id: string,
    @Args('data', { type: () => CommentHttpUpdate }, ValidationPipe, NotEmptyObjectPipe) data: CommentHttpUpdate,): Promise<CommentEntity> {
    return this.commentService.updateComment(id, data);
  }

  @Mutation(() => CommentHttp)
  removeComment(@Args('id', { type: () => String }, ParseUUIDPipe) id: string): Promise<CommentEntity> {
    return this.commentService.deleteComment(id);
  }

  @ResolveField(() => PostHttp, {
    name: 'post',
    nullable: false,
    description: "Post of comment"
  })
  resolvePost(
    @Parent() comment: CommentHttp
  ) {
    return this.commentService.findPost(comment.id);
  }

  @ResolveField(() => UserHttp, {
    name: 'user',
    nullable: false,
    description: "User of comment"
  })
  resolveUser(
    @Parent() comment: CommentHttp
  ) {
    return this.commentService.findUser(comment.id);
  }
}
