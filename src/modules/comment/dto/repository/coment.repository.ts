import { PostRepository } from "../../../post/dto/repository/post.repository";
import { UserRepository } from "../../../user/dto/repository/user.repository";

export class CommentRepository {
  id!: string;
  comment!: string;
}
