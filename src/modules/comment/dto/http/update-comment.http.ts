import { Field, PartialType, InputType } from '@nestjs/graphql';
import { CommentHttpCreate } from './create-comment.http';
import { IsNotEmpty, IsOptional, IsString, IsUUID } from "class-validator";

@InputType()
export class CommentHttpUpdate extends PartialType(CommentHttpCreate) {
  @Field(() => String, {
    description: 'comment of the user',
    nullable: true,
  })
  @IsString()
  @IsOptional()
  comment?: string;
}