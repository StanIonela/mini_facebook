import { Field, InputType } from '@nestjs/graphql';
import { IsNotEmpty, IsString } from 'class-validator';

@InputType()
export class CommentHttpCreate {
  @Field(() => String, { description: 'id of the comment', nullable: true })
  id!: string;

  @Field(() => String, {
    description: 'comment of the user',
    nullable: false,
  })
  @IsString()
  @IsNotEmpty()
  comment!: string;

  @Field(() => String)
  userId!: string;

  @Field(() => String)
  postId!: string;
}
