import { Field, ObjectType } from '@nestjs/graphql';
import { IsNotEmpty, IsString } from 'class-validator'

@ObjectType('Comment')
export class CommentHttp {
  @Field(() => String, { description: 'id of the comment', nullable: true })
  id!: string;

  @Field(() => String, {
    description: 'comment of the user',
    nullable: false,
  })
  @IsString()
  @IsNotEmpty()
  comment!: string;
}
