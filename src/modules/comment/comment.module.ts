import { Module } from '@nestjs/common';
import { CommentService } from './comment.service';
import { CommentResolver } from './comment.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommentEntity } from "./entities/comment.entity";
import { PostModule } from "../post/post.module";

@Module({
  imports: [TypeOrmModule.forFeature([CommentEntity]), PostModule],
  providers: [CommentResolver, CommentService],
})
export class CommentModule {}
