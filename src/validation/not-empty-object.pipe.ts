import { ArgumentMetadata, BadRequestException, PipeTransform } from "@nestjs/common";

export class NotEmptyObjectPipe implements PipeTransform {
  transform(value: any, metadata: ArgumentMetadata) {
    if(Object.keys(value).length === 0){
      throw new BadRequestException(`Object can not be empty`);
    }
    return value;
  }
}